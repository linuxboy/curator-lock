package com.tastycai.curatorlock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuratorLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(CuratorLockApplication.class, args);
    }

}
